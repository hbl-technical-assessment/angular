# Technical assessment - Angular

## Run Locally

Run `git clone https://gitlab.com/hbl-technical-assessment/angular.git && cd angular` to clone the project

Run `npm install` to ensure that npm modules are installed

Run `ng serve --open` to start local server and open the angular app in your browser.

**The form is visible at the http://localhost:4200/user-profile url.**
