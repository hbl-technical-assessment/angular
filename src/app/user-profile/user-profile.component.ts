import { Component, OnInit, inject } from '@angular/core';
import { UserService } from '../user.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})

export class UserProfileComponent implements OnInit {
  name: string = "";
  email: string = "";
  userService: UserService = inject(UserService);

  constructor() { 
    this.userService.getUserData().then(data => {
      this.name = data.name;
      this.email = data.email;
    });
  }

  ngOnInit() {
   
  }

  updateEmail() {
    this.userService.updateEmail(this.email).then(() => {
      console.log('Email updated successfully');
    }).catch(() => {
      console.log('Error updating email');
    });
  }
}
