import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  async getUserData() {
    const data = await fetch('https://jsonplaceholder.typicode.com/users/1');
    return await data.json() ?? [];
  }

  async updateEmail(email: string) {
    const url = 'https://mock-api.com/update-email';
    const data = {
      email
    };
  
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
  
    return response.status === 200;
  }
}
